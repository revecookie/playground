object NumWays extends App{

  /**
    * The idea is that the you can reach to step N from specific steps that are dictated by x.
    * For example: N=5 and x = {1, 2}
    * That means that you can reach step 5 either from step 5 - 1 = 4 or from 5 - 2 = 3
    * so reach(5) = reach(4) + reach(3)
    * Then you can calculate reach(4). You can reach step 4 either from step 4 - 1 = 3 or from 4 - 2 = 2
    * so reach(4) = reach(3) + reach(2) and so on...
    * @param n number of steps
    * @param x set of available paces
    * @return the number of ways to ascend the staircase
    */
  def numWays(n: Int, x: Set[Int]): Int = {
    var preCalc = scala.collection.mutable.Map[Int, Int]()
    for(step:Int <- 1 to n) {
      var comb = 0
      for(pace: Int <- x) {
        if(step ==  pace) comb = comb + 1
        else if (step > pace) comb = comb + preCalc(step - pace)
      }
      preCalc(step) = comb
    }

    preCalc(n)
  }

  println(numWays(4, Set(1,2))) //should be 5
  println(numWays(5, Set(1,2))) //should be 8
  println(numWays(4, Set(1,3))) //should be 3

}
